# Before started
* Stop all running services on your machine or change services port in .env file
```
sudo service mysql stop
sudo service apache2 stop
```

# Docker project
* Install project
```
docker-compose build
docker-compose up -d
```

# Project initialization
* Connect to PHP container and install project dependencies
```
docker exec -it php bash
composer install
```

* Generate database tables
```
bin/console doctrine:schema:update --force
```

* Generate ssh keys for jwt
```
mkdir -p config/jwt
openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
```

# Project informations
* Get available routes
```
bin/console debug:router
```
* Get containers list
```
docker container ls
```
* Connect to docker container
```
docker exec -it <container_name> bash
```
* Access to mysql
```
Mysql: http://localhost:8081
login=app
password=app
```

# Api
* User registration 
```
http://localhost:8080/api/register [POST]
Content-Type: application/json
Body { username, password }
```

* User login that returns a jwt token
```
http://localhost:8080/api/login [POST]
Content-Type: application/json
Body { username, password }
```

* Access to logged user route with token
```
http://localhost:8080/api/hello [GET]
Authorization Bearer <token>
```
