<?php

namespace App\Controller;

use App\Entity\Job;
use App\Entity\Project;
use App\Entity\User;
use App\Service\Serializer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class JobController extends AbstractController
{
    private $entityManager;
    private $serializer;
    private $validator;

    public function __construct(
        EntityManagerInterface $entityManager,
        Serializer $serializer,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * @Route("/job", name="job_add", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function addJob(Request $request)
    {
        $user = $this->getUser();

        $job = $this->serializer->deserialize($request->getContent(), Job::class);
        $user->addJob($job);

        $this->entityManager->persist($job);
        $this->entityManager->flush();

        $job = $this->serializer->normalize($job, 'json', [
            'groups' => ['api']
        ]);

        return $this->json([
            'job' => $job
        ], 200);
    }

    /**
     * @Route("/job/{id}/project", name="project_add", methods={"POST"})
     * @param Job $job
     * @param Request $request
     * @return JsonResponse
     */
    public function addProject(Job $job, Request $request)
    {
        $user = $this->getUser();
        $project = $this->serializer->deserialize($request->getContent(), Project::class);

        if (!$user->getJobs()->contains($job)) {
            return $this->json([
                'message' => 'Job not found'
            ], 404);
        }

        $job->addProject($project);

        $this->entityManager->persist($project);
        $this->entityManager->flush();

        $project = $this->serializer->normalize($project, 'json', [
            'groups' => ['api']
        ]);

        return $this->json([
            'project' => $project
        ], 200);
    }
}
