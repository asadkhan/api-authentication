<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;


class SecurityController extends AbstractFOSRestController
{
    private $em;
    private $passwordEncoder;

    public function __construct(
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Rest\Post(path="/register", name="register")
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        try{
            $user = new User();
            $user->setEmail($data['username']);
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $data['password']
            ));

            $this->em->persist($user);
            $this->em->flush();
        }catch (UniqueConstraintViolationException $e){
            return $this->json([
                "status" => 403,
                "message" => "L'email est deja utilisé"
            ], 403);
        }

        return $this->json([
            "status" => 200,
            "message" => "Votre compte a bien été créé, veuillez vous connecter"
        ], 200);
    }
}
