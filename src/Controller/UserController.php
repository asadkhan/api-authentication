<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\Entity\UserService;
use App\Service\Serializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends AbstractController
{
    private $userService;
    private $serializer;
    private $validator;

    public function __construct(
        UserService $userService,
        Serializer $serializer,
        ValidatorInterface $validator
    ) {
        $this->userService = $userService;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * @Route("/user", name="user_get", methods={"GET"})
     * @return JsonResponse
     */
    public function user()
    {
        $user = $this->getUser();

        $user = $this->serializer->normalize($user, 'json', [
            'groups' => ['api']
        ]);

        return $this->json([
            'user' => $user
        ], 200);
    }

    /**
     * @Route("/user", name="user_edit", methods={"PUT"})
     * @param Request $request
     * @return JsonResponse
     */
    public function editUser(Request $request)
    {
        $user = $this->getUser();
        $newUser = $this->serializer->deserialize($request->getContent(),  User::class, 'json');
        $this->userService->build($newUser);
        $this->userService->update($user, $newUser);

        $this->userService->beginTransaction();
        $this->userService->persist($user);
        $this->userService->flush();
        $this->userService->commit();

        $user = $this->serializer->normalize($user, 'json', [
            'groups' => ['api']
        ]);

        return $this->json([
            'user' => $user
        ], 200);
    }

    /**
     * @Route("/user/phone", name="user_phone_edit", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function editPhone(Request $request)
    {
        $user = $this->getUser();
        $data = $this->serializer->deserialize($request->getContent(),  User::class, 'json');

        $validations = $this->validator->validate($data, null, ['phone']);

        if (count($validations) > 0) {
            $errors = [];
            foreach ($validations as $validation) {
                $errors['form'][$validation->getPropertyPath()] = $validation->getMessage();
            }

            return $this->json([
                'success' => false,
                'errors' => $errors
            ], 403);
        }

        if (null !== $data->getPhoneNumber()) {
            $user->setPhoneNumber($data->getPhoneNumber());
        }

        if (null !== $data->getMobileNumber()) {
            $user->setMobileNumber($data->getMobileNumber());
        }

        $this->userService->beginTransaction();
        $this->userService->persist($user);
        $this->userService->flush();
        $this->userService->commit();

        return $this->json([
            'success' => true,
            'payload' => [
                'id' => $user->getId(),
                'email' => $user->getEmail(),
                'phone_number' => $user->getPhoneNumber(),
                'mobile_number' => $user->getMobileNumber()
            ]
        ], 200);
    }
}
