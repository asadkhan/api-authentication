<?php

namespace App\Listener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

class LogoutSuccessListener implements LogoutSuccessHandlerInterface
{
    /**
     * Creates a Response object to send upon a successful logout.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function onLogoutSuccess(Request $request)
    {
        return new JsonResponse([
            'message' => "Déconnexion réussie"
        ], 200);
    }
}