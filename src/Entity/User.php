<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Migrations\Tools\Console\Exception\VersionAlreadyExists;
use Doctrine\ORM\Mapping as ORM;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Serializer\Annotation\Groups;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"api"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"api"})
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="phone_number", length=255, nullable=true)
     * @Type("libphonenumber\PhoneNumber")
     * @Groups({"api"})
     * @AssertPhoneNumber(type="fixed_line", defaultRegion="FR", groups={"phone"})
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="phone_number", length=255, nullable=true)
     * @Type("libphonenumber\PhoneNumber")
     * @Groups({"api"})
     * @AssertPhoneNumber(type="mobile", defaultRegion="FR", groups={"phone"})
     */
    private $mobileNumber;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Job", mappedBy="user")
     * @Groups({"api"})
     */
    private $jobs;

    public function __construct()
    {
        $this->jobs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        //$roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPhoneNumber(): ?string
    {
        if (null === $this->phoneNumber) return null;

        $phoneUtil = $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        return $phoneUtil->format($this->phoneNumber, \libphonenumber\PhoneNumberFormat::NATIONAL);
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $phoneUtil = $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        try {
            $this->phoneNumber = $phoneUtil->parse($phoneNumber, 'FR');
        } catch (NumberParseException $e) {
            $this->phoneNumber = null;
        }

        return $this;
    }

    public function getMobileNumber(): ?string
    {
        if (null === $this->mobileNumber) return null;

        $phoneUtil = $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        return $phoneUtil->format($this->mobileNumber, \libphonenumber\PhoneNumberFormat::NATIONAL);
    }

    public function setMobileNumber(?string $mobileNumber): self
    {
        $phoneUtil = $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        try {
            $this->mobileNumber = $phoneUtil->parse($mobileNumber, 'FR');
        } catch (NumberParseException $e) {
            $this->mobileNumber = null;
        }

        return $this;
    }

    /**
     * @return Collection|Job[]
     */
    public function getJobs(): Collection
    {
        return $this->jobs;
    }

    public function setJobs(Collection $jobs): self
    {
        $this->jobs = $jobs;
        return $this;
    }

    public function addJob(Job $job): self
    {
        if (!$this->jobs->contains($job)) {
            $this->jobs[] = $job;
            $job->setUser($this);
        }

        return $this;
    }

    public function removeJob(Job $job): self
    {
        if ($this->jobs->contains($job)) {
            $this->jobs->removeElement($job);
            // set the owning side to null (unless already changed)
            if ($job->getUser() === $this) {
                $job->setUser(null);
            }
        }

        return $this;
    }

    public function setObject(User $user): self
    {
        if (null !== $user->getPhoneNumber())
        {
            $this->setPhoneNumber($user->getPhoneNumber());
        }

        if (null !== $user->getMobileNumber())
        {
            $this->setMobileNumber($user->getMobileNumber());
        }

        if (null !== $user->getJobs() && count($user->getJobs()) > 0)
        {
            $this->jobs = $user->getJobs();
            /*foreach ($user->getJobs() as $job) {
                $this->addJob($job);
            }*/
        }

        return $this;
    }
}
