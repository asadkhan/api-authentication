<?php

namespace App\Service\Entity;

use App\Entity\Job;
use App\Entity\Project;
use App\Repository\JobRepository;
use Doctrine\ORM\EntityManagerInterface;

class JobService extends EntityService
{
    private $projectService;

    public function __construct(
        EntityManagerInterface $entityManager,
        JobRepository $repository,
        ProjectService $projectService
    ) {
        parent::__construct($entityManager, $repository);
        $this->projectService = $projectService;
    }

    public function build($object): Job
    {
        if (null === $object) {
            return null;
        }

        if (null !== $object->getProjects() && count($object->getProjects()) > 0) {
            foreach ($object->getProjects() as $key => $project) {
                $object->getProjects()[$key] = $this->projectService->build($project);
            }
        }

        if (null !== $object->getId()) {
            $job = $this->find($object->getId());
            if (null !== $job) {
                $object = $job->setObject($object);
            }
        }

        return $object;
    }

    public function update($job, $data): Job
    {
        $projects = $data->getProjects();
        if (null !== $projects && count($projects) > 0) {
            foreach ($projects as $key => $project) {
                $existProject = $this->projectService->findUserProjectById($job->getId(), $project->getId());
                if (null !== $existProject) {
                    $projects[$key] = $this->projectService->update($existProject, $project);
                } else {
                    $projects[$key] = $this->projectService->update(new Project(), $project);
                }
                $projects[$key]->setJob($job);
                $this->entityManager->persist($projects[$key]);
            }
            $data->setProjects($projects);
        }

        $job->setObject($data);
        return $job;
    }

    public function findUserJobById(?int $userId, ?int $jobId)
    {
        return $this->repository->findUserJobById($userId, $jobId);
    }
}