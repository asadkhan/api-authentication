<?php


namespace App\Service\Entity;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

abstract class EntityService
{
    protected $entityManager;
    protected $repository;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntityRepository $repository
    ) {
        $this->entityManager = $entityManager;
        $this->repository = $repository;
    }

    public function findAll()
    {
        return $this->repository->findAll();
    }

    public function find($id, $lockMode = null, $lockVersion = null)
    {
        return $this->repository->find($id, $lockMode, $lockVersion);
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->repository->findOneBy($criteria, $orderBy);
    }

    public function count(array $criteria)
    {
        return $this->repository->count($criteria);
    }

    public function clear()
    {
        $this->repository->clear();
    }

    public function beginTransaction()
    {
        $this->entityManager->beginTransaction();
    }

    public function persist($object)
    {
        try {
            $this->entityManager->persist($object);
        } catch (\Exception $e) {
            $this->rollback();
        }
    }

    public function flush()
    {
        try {
            $this->entityManager->flush();
        } catch (\Exception $e) {
            $this->rollback();
        }
    }

    public function commit()
    {
        $this->entityManager->commit();
    }

    public function rollback()
    {
        $this->entityManager->rollback();
    }

    public abstract function build($object);
    public abstract function update($object, $data);
}