<?php

namespace App\Service\Entity;

use App\Entity\Project;
use App\Repository\ProjectRepository;
use Doctrine\ORM\EntityManagerInterface;

class ProjectService extends EntityService
{
    public function __construct(
        EntityManagerInterface $entityManager,
        ProjectRepository $repository
    ) {
        parent::__construct($entityManager, $repository);
    }

    public function build($object): Project
    {
        if (null === $object) {
            return null;
        }

        if (null !== $object->getId()) {
            $project = $this->find($object->getId());
            if (null !== $project) {
                $object = $project->setObject($object);
            }
        }

        return $object;
    }

    public function update($project, $data): Project
    {
        $project->setObject($data);
        return $project;
    }

    public function findUserProjectById(?int $jobId, ?int $projectId)
    {
        return $this->repository->findUserProjectById($jobId, $projectId);
    }
}