<?php

namespace App\Service\Entity;

use App\Entity\Job;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;

class UserService extends EntityService
{
    private $jobService;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserRepository $repository,
        JobService $jobService
    ) {
        parent::__construct($entityManager, $repository);
        $this->jobService = $jobService;
    }

    public function build($object) : User
    {
        if (null === $object) {
            return null;
        }

        if (null !== $object->getJobs() && count($object->getJobs()) > 0) {
            foreach ($object->getJobs() as $key => $job) {
                $object->getJobs()[$key] = $this->jobService->build($job);
            }
        }

        if (null !== $object->getId()) {
            $user = $this->find($object->getId());
            if (null !== $user) {
                $object = $user->setObject($object);
            }
        }

        return $object;
    }

    public function update($user, $data): User
    {
        $jobs = $data->getJobs();
        if (null !== $jobs && count($jobs) > 0) {
            foreach ($jobs as $key => $job) {
                $existJob = $this->jobService->findUserJobById($user->getId(), $job->getId());
                if (null !== $existJob) {
                    $jobs[$key] = $this->jobService->update($existJob, $job);
                } else {
                    $jobs[$key] = $this->jobService->update(new Job(), $job);
                }
                $jobs[$key]->setUser($user);
                $this->entityManager->persist($jobs[$key]);
            }
            $data->setJobs($jobs);
        }

        $user->setObject($data);
        return $user;
    }
}