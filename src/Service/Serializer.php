<?php


namespace App\Service;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer as SerializerService;

class Serializer
{
    private $entityManager;
    private $serializer;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));

        $dateCallback = function ($innerObject, $outerObject, string $attributeName, string $format = null, array $context = []) {
            return ($innerObject instanceof \DateTime or $innerObject instanceof \DateTimeImmutable) ? $innerObject->format('Y-m-d') : null;
        };

        $defaultContext = [
            AbstractNormalizer::CALLBACKS => [
                'startDate' => $dateCallback,
                'endDate' => $dateCallback
            ],
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            }
        ];

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [
            new ObjectNormalizer(
                $classMetadataFactory,
                new CamelCaseToSnakeCaseNameConverter(),
                null,
                new ReflectionExtractor(),
                null,
                null,
                $defaultContext
            ),
            new GetSetMethodNormalizer(
                null,
                null,
                null,
                null,
                null,
                $defaultContext
            ),
            new ArrayDenormalizer(),
            new DateTimeNormalizer()
        ];

        $this->serializer = new SerializerService($normalizers, $encoders);
    }

    public function serialize($data, $format = 'json', array $context = [])
    {
        return $this->serializer->serialize($data, $format, $context);
    }

    public function deserialize($data, $type, $format = 'json', array $context = [])
    {
        return $this->serializer->deserialize($data, $type, $format, $context);
    }

    public function normalize($data, $format = 'json', array $context = [])
    {
        try {
            return $this->serializer->normalize($data, $format, $context);
        } catch (ExceptionInterface $e) {
            return $e->getMessage();
        }
    }

    public function denormalize($data, $type, $format = 'json', array $context = [])
    {
        try {
            return $this->serializer->denormalize($data, $type, $format, $context);
        } catch (ExceptionInterface $e) {
            return $e->getMessage();
        }
    }
}